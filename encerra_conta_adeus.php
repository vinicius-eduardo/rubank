<?php
include('verifica_login.php');
include('verifica_encerramento.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Saldo</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/bulma.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/login.css">
</head>
<body>
	<section class="hero is-success is-fullheight">

		<div class="hero-body">

			<div class="column is-8 is-offset-2">
				
				<div class="box">

					<div class="field">

						<section class="hero is-medium is-info">

							<div class="hero-body">

								<div class="container">

									<h1 class="title is-1">
										Lamentamos sua saída.
									</h1>
									<h2 class="subtitle is-2">
										Agradecemos por ter utilizado nossos serviços, até logo!
									</h2>

									<?php if(isset($_SESSION['reembolso_id'])): ?>
									<p>
										Utilize o código para fazer a retirada da quantia: 
										<?php echo $_SESSION['reembolso_id'] ?>
									</p>
									<?php endif; ?>
								</div>

							</div>

						</section>

					</div>

					<div class="field">
						<a href="index.php" class="button is-link is-medium is-fullwidth">
							Ir Para Tela Inicial
						</a>
					</div>
					
				</div>

			</div>

		</div>

	</section>
	<?php 
	unset($_SESSION['excluido']);
	unset($_SESSION['reembolso_id']);
	session_start();
	session_destroy();
	?>
</body>
</html>