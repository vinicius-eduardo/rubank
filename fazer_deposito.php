<?php
session_start();
include('conexao.php');

$location = "Location: deposito.php";

if( empty($_POST['conta']) || empty($_POST['cpf']) || empty($_POST['quantia'])){
	$_SESSION['campo_vazio'] = true;
	header($location);
	exit;
}

$conta = mysqli_real_escape_string($conexao, trim($_POST['conta']));
$cpf = mysqli_real_escape_string($conexao, trim($_POST['cpf']));
$quantia = mysqli_real_escape_string($conexao, $_POST['quantia']);

$sql = "select * from cliente where id_cliente = '$conta' and cpf = '$cpf'"; 
$result = mysqli_query($conexao, $sql);

if(!$result){
	$_SESSION['nao_encontrado'] = true;
	header($location);
	exit;
}

$sql = "update conta set saldo = saldo + '$quantia', ultimo_deposito = '$quantia' where id_cliente = '$conta'";
$result = mysqli_query($conexao, $sql);

if($result){
	$_SESSION['concluido'] = true;
	header($location);
}else{
	header('Location: painel_fiscal.php');
}
?>