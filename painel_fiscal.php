<?php
include('verifica_login.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Painel Fiscal - RU Bank</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/bulma.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/login.css">
</head>

<body>
	<section class="hero is-success is-fullheight">

		<div class="hero-body">

			<div class="column is-4 is-offset-4 has-text-centered">
				
				<h3 class="title has-text-grey">
					Fiscal: <?php echo $_SESSION['nome']; ?>
				</h3>
				<h3 class="title has-text-grey">Selecione uma Opção:</h3>
				
				<div class="box">

					<div class="field">

						<p class="control">
							<a href="pagamento.php" class="button is-block is-link is-large is-fullwidth">Pagamento</a>
						</p>

					</div>

					<div class="field">

						<p class="control">
							<a href="deposito.php" class="button is-block is-link is-large is-fullwidth">Depósito</a>
						</p>

					</div>

					<div class="field">

						<p class="control">
							<a href="reembolso.php" class="button is-block is-link is-large is-fullwidth">Reembolso</a>
						</p>

					</div>

					<div class="field">

						<p class="control">
							<a href="logout.php" class="button is-danger is-fullwidth">Sair</a>
						</p>

					</div>

				</div>

			</div>

		</div>
		
	</section>
</body>
</html>

