<?php
include('verifica_login.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Saldo</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/bulma.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/login.css">
</head>

<body>
	<section class="hero is-success is-fullheight">
		
		<div class="hero-body">

			<div class="column is-8 is-offset-2">
				
				<div class="box">
					
					<div class="field">
						
						<section class="hero is-small is-info is-bold">

							<div class="hero-body">

								<div class="container has-text-centered">

									<h1 class="title">Você Está Prestes a Fechar Sua conta</h1>
									<h2 class="subtitle">O que deseja fazer</h2>
								
								</div>

							</div>

						</section>

					</div>
					
					<form action="encerrar.php" method="post">

						<div class="field is-grouped">
							
							<p class="control is-expanded">
								<a href="encerra_conta_transferencia.php" class="button is-link is-fullwidth is-large">
									Transferir
								</a>
							</p>

							<p class="control is-expanded">
								<button type="submit" class="button is-link is-fullwidth is-large">Sacar</button>	
							</p>
							
							<input type="hidden" name="encerramento" value="checked" checked>
						</div>

						<div class="field">
							<p class="control">
								<a href="painel.php" class="button is-fullwidth is-medium is-danger">Cancelar</a>
							</p>
						</div>

					</form>
					
				</div>

			</div>

		</div>

	</section>
</body>
</html>