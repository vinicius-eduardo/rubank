<?php
session_start();
include('conexao.php');

$location = "Location: reembolso.php";

if( empty($_POST['conta']) || empty($_POST['cpf']) || empty($_POST['idreembolso'])){
	$_SESSION['campo_vazio'] = true;
	header($location);
	exit;
}

$conta = mysqli_real_escape_string($conexao, trim($_POST['conta']));
$cpf = mysqli_real_escape_string($conexao, trim($_POST['cpf']));
$id_reembolso = mysqli_real_escape_string($conexao, $_POST['idreembolso']);

$sql = "select count(*) as total from reembolso where id_reembolso = '$id_reembolso' and conta = '$conta' and cpf = '$cpf'"; 
$result = mysqli_query($conexao, $sql);
$row = mysqli_fetch_assoc($result);

if($row['total'] != 1){
	$_SESSION['nao_encontrado'] = true;
	header($location);
	exit;
}

$sql = "select valor from reembolso where id_reembolso = '$id_reembolso'"; 
$result = mysqli_query($conexao, $sql);
$row = mysqli_fetch_assoc($result);

$sql = "delete from reembolso where id_reembolso = '$id_reembolso'";
$result = mysqli_query($conexao, $sql);

if($result){
	$_SESSION['valor'] = $row['valor'];	
	$_SESSION['concluido'] = true;
	header($location);
}else{
	header('Location: painel_fiscal.php');
}


?>