<?php
include('verifica_login.php');
include('consultar_saldo.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Saldo</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/bulma.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/login.css">
</head>
<body>
<section class="hero is-success is-fullheight">
			<div class="hero-body">
				<div class="container">
					<div class="column is-8 is-offset-2">
						
						<div class="box">
							<div class="field">
								<section class="hero is-medium is-primary is-bold">
									<div class="hero-body">
										<div class="container">
											<h1 class="title is-1">
											R$ 
											<?php
											echo $_SESSION['saldo'];
											unset($_SESSION['saldo']);
											?>
											</h1>
											<h2 class="subtitle is-2">
											Saldo </h2>

										</div>
									</div>
								</section>
							</div>
							
							<div class="field">
								<p class="control">
									<a href="painel.php" class="button is-fullwidth is-medium">Voltar</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
</body>
</html>