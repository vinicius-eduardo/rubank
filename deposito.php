<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>RU Bank</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/bulma.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/login.css">
</head>

<body>
	<section class="hero is-success is-fullheight">

		<div class="hero-body">

			<div class="container has-text-centered">

				<div class="column is-4 is-offset-4">
					
					<h3 class="title has-text-grey">Depósito</h3>
					<h3 class="title has-text-grey">RU Bank</h3>
					
					<?php
					if(isset($_SESSION['nao_encontrado'])):
					?>
					
					<div class="notification is-danger">
						<p>ERRO: Conta Não Encontrada.</p>
					</div>

					<?php
					endif;
					unset($_SESSION['nao_encontrado']);
					?>

					<?php
					if(isset($_SESSION['concluido'])):
					?>
					
					<div class="notification is-success">
						<p>Pagamento Concluido.</p>
					</div>

					<?php
					endif;
					unset($_SESSION['concluido']);
					?>

					<?php
					if(isset($_SESSION['campo_vazio'])):
					?>
					
					<div class="notification is-danger">
						<p>ERRO: Preencha o campo.</p>
					</div>

					<?php
					endif;
					unset($_SESSION['campo_vazio']);
					?>
					
					<div class="box">
						<form action="fazer_deposito.php" method="POST">
							
							<div class="field">
								<div class="control">
									<input name="conta" name="text" class="input is-large" placeholder="Conta" autofocus="">
								</div>
							</div>

							<div class="field has-addons">
								<p class="control">
									<a class="button is-static is-medium">R$</a>
								</p>
								<p class="control is-expanded">
									<input class="input is-medium" name="quantia" type="number" step='any' min='0.1' placeholder="5,00">
								</p>
							</div>

							<div class="field">
								<div class="control">
									<input name="cpf" name="text" class="input is-large" placeholder="CPF da Conta" autofocus="">
								</div>
							</div>
							
							<div class="field">
								<button type="submit" class="button is-block is-warning is-large is-fullwidth">Concluir</button>
							</div>
							
							<div class="field">
								<a href="painel_fiscal.php" class="button is-fullwidth">Voltar</a>
							</div>
							
						</form>

					</div>

				</div>

			</div>

		</div>

	</section>

</body>
</html>