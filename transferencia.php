<?php
include('verifica_login.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Transferências</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/bulma.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/login.css">
</head>
<body>
	
	<section class="hero is-success is-fullheight">
		<div class="hero-body">
			<div class="container has-text-centered">
				<div class="column is-4 is-offset-4">

					<?php
						if(isset($_SESSION['sem_dados'])):
					?>
					<div class="notification is-danger">
						<p>
							ERRO: Campos Faltando.
						</p>
					</div>
					<?php
						endif;
						unset($_SESSION['sem_dados']);
					?>

					<?php
						if(isset($_SESSION['usuario_inexistente'])):
					?>
					<div class="notification is-danger">
						<p>
							ERRO: O Usuário favorecido não existe.
						</p>
					</div>
					<?php
						endif;
						unset($_SESSION['usuario_inexistente']);
					?>

					<?php
						if(isset($_SESSION['saldo_indisponivel'])):
					?>
					<div class="notification is-danger">
						<p>
							ERRO: Saldo Insuficiente.
						</p>
					</div>
					<?php
						endif;
						unset($_SESSION['saldo_indisponivel']);
					?>

					<?php
						if(isset($_SESSION['sucesso_transacao'])):
					?>
					<div class="notification is-success">
						<p>
							Transação bem-sussedida.
						</p>
					</div>
					<?php
						endif;
						unset($_SESSION['sucesso_transacao']);
					?>

					
					<div class="box">
						<form action="transferir.php" method="POST">
							<div class="field">
								<div class="control">
									<label class="label">Favorecido</label>
									<input name="favorecido" type="text" class="input is-large" placeholder="Nome Completo" autofocus>
								</div>
							</div>

							<div class="field">
								<div class="control">
									<label class="label">Conta do Favorecido</label>
									<input name="conta_favorecido" type="number" class="input is-large" placeholder="Conta" autofocus>
								</div>
							</div>

							<div class="field has-addons">
								<p class="control">
									<a class="button is-static is-medium">R$</a>
								</p>
								<p class="control is-expanded">
									<input class="input is-medium" name="quantia" type="number" step='any' min='0.1' placeholder="5,00">
								</p>
							</div>

							<div class="field">
								<div class="control">
									<input name="senha" class="input is-large" type="password" placeholder="Sua senha">
								</div>
							</div>

							<div class="field">
								<button type="submit" class="button is-block is-link is-large is-fullwidth">Transferir</button>
							</div>
							
							<div class="field">
								<a href="painel.php" class="button is-fullwidth">Voltar</a>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>