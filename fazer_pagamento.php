<?php
session_start();
include('conexao.php');

$location = "Location: pagamento.php";

if( empty($_POST['conta']) || empty($_POST['cpf'])){
	$_SESSION['campo_vazio'] = true;
	header($location);
	exit;
}

$conta = mysqli_real_escape_string($conexao, trim($_POST['conta']));
$cpf = mysqli_real_escape_string($conexao, trim($_POST['cpf']));

$sql = "select * from cliente where id_cliente = '$conta' and cpf = '$cpf'"; 
$result = mysqli_query($conexao, $sql);

if(!$result){
	$_SESSION['nao_encontrado'] = true;
	header($location);
	exit;
}

$row = mysqli_fetch_assoc($result);
$cat = $row['categoria'];

$sql = "select valor from categoria where funcao = '$cat'";
$result = mysqli_query($conexao, $sql);
$row = mysqli_fetch_assoc($result);
$quantia = $row['valor'];

$sql = "select saldo from conta where id_cliente = '$conta'";
$result = mysqli_query($conexao, $sql);
$row = mysqli_fetch_assoc($result);
$saldo = $row['saldo'];

$_SESSION['saldo'] = $saldo;
$_SESSION['quantia'] = $quantia;

if(floatval($quantia) > floatval($saldo)){
	$_SESSION['saldo_insuficiente'] = true;
	header($location);
	exit;
}

$sql = "update conta set saldo = saldo - '$quantia', ultimo_pagamento = '$quantia' where id_cliente = '$conta'";
$result = mysqli_query($conexao, $sql);

if($result){
	$_SESSION['concluido'] = true;
	header($location);
}else{
	header('Location: painel_fiscal.php');
}
?>