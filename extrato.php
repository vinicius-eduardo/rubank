<?php
include('verifica_login.php');
include('consultar_extrato.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Saldo</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/bulma.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/login.css">
</head>

<body>
	<section class="hero is-success is-fullheight">
		
		<div class="hero-body">

			<div class="column is-8 is-offset-2">
				
				<div class="box">
					
					<div class="field">
						
						<section class="hero is-small is-info is-bold">

							<div class="hero-body">

								<div class="container has-text-centered">

									<p class="title is-5">
										Saldo: R$ <?php echo $_SESSION['saldo']; ?>
									</p>
									
									<p class="title is-5">
										Número da Conta: <?php echo $_SESSION['cliente_id']; ?>
									</p>

									<p class="title is-5">
										Último Pagamento: <?php echo $_SESSION['ultimo_pagamento']; ?>
									</p>

									<p class="title is-5">
										Último Depósito:  <?php echo $_SESSION['ultimo_deposito']; ?>
									</p>

									<p class="title is-5">
										ID extrato: <?php echo $_SESSION['extrato_id']; ?>
									</p>

									<p class="title is-5">
										Data: <?php echo $_SESSION['data']; ?>
									</p>

									<p class="title is-5">
										Obs: <?php echo $_SESSION['msg']; ?>
									</p>

								</div>

							</div>

						</section>

					</div>
					
					<div class="field">
						<p class="control">
							<a href="painel.php" class="button is-fullwidth is-medium">Voltar</a>
						</p>
					</div>

				</div>

			</div>

		</div>

	</section>

	<?php
		unset($_SESSION['saldo']);
		unset($_SESSION['ultimo_pagamento']);
		unset($_SESSION['ultimo_deposito']);
		unset($_SESSION['extrato_id']);
		unset($_SESSION['data']);
		unset($_SESSION['msg']);
		unset($_SESSION['extrato_gerado']);
	?>
</body>
</html>