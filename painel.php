<?php
include('verifica_login.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Painel - RU Bank</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/bulma.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/login.css">
</head>

<body>
	<section class="hero is-success is-fullheight">

		<div class="hero-body">

			<div class="column is-4 is-offset-4 has-text-centered">
				
				<h3 class="title has-text-grey">
					Olá, <?php echo $_SESSION['nome'];?>
				</h3>
				<h3 class="title has-text-grey">O que deseja fazer:</h3>
				
				<div class="box">

					<div class="field">

						<p class="control">
							<a href="saldo.php" class="button is-block is-link is-large is-fullwidth">Consultar Saldo</a>
						</p>

					</div>

					<div class="field">

						<p class="control">
							<a href="transferencia.php" class="button is-block is-link is-large is-fullwidth">Transferência</a>
						</p>

					</div>

					<div class="field">

						<p class="control">
							<a href="extrato.php" class="button is-block is-link is-large is-fullwidth">Extrato</a>
						</p>

					</div>

					<div class="field">

						<p class="control">
							<a href="encerra_conta_confirmacao.php" class="button is-block is-link is-large is-fullwidth">
								Fechar Conta
							</a>
						</p>

					</div>

					<div class="field">

						<p class="control">
							<a href="logout.php" class="button is-danger is-fullwidth">Sair</a>
						</p>

					</div>

				</div>

			</div>

		</div>
		
	</section>
</body>
</html>

