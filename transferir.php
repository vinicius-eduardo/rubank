<?php
session_start();
include('conexao.php');

if(isset($_POST['encerramento'])){
	$location = "Location: encerra_conta_transferencia.php";
	if($_POST['confirma'] != '1'){
		$_SESSION['erro_confirmacao'] = true;
		header($location);
		exit;
	}
}else{
	$location = "Location: transferencia.php";
}


//deixa passar falta da quantia se for encerramento de conta
if( (!isset($_POST['encerramento']) & empty($_POST['quantia'])) ||
	(empty($_POST['favorecido']) || empty($_POST['conta_favorecido']) || empty($_POST['senha'])) ){

	$_SESSION['sem_dados'] = true;	//msg erro campos faltando
	header($location);
	exit;

}

if( !isset($_POST['encerramento']) ){
	$quantia = mysqli_real_escape_string($conexao, $_POST['quantia']);
}

$nomeFav = mysqli_real_escape_string($conexao, trim($_POST['favorecido']));
$contaFav = mysqli_real_escape_string($conexao, $_POST['conta_favorecido']);
$senha = mysqli_real_escape_string($conexao, trim(md5($_POST['senha'])));
$conta = $_SESSION['cliente_id'];

$sql = "select count(*) as total from cliente where id_cliente = '$contaFav' and nome = '$nomeFav'"; 
$result = mysqli_query($conexao, $sql);
$row = mysqli_fetch_assoc($result);

if($row['total'] != 1) {
	$_SESSION['usuario_inexistente'] = true;	//erro favorecido inexistente
	header($location);
	exit;
}

$sql = "select saldo from conta where id_cliente = '$conta'"; 
$result = mysqli_query($conexao, $sql);
$row = mysqli_fetch_assoc($result);
$saldo = $row['saldo'];

if(isset($_POST['encerramento'])){
	$quantia = $row['saldo'];
}else{
	if(floatval($quantia) > floatval($saldo)){
		$_SESSION['saldo_indisponivel'] = true;	//erro saldo
		header($location);
		exit;
	}
}

$sql = "update conta set saldo = saldo - '$quantia' where id_cliente = '$conta';"; 
$sql .= "update conta set ultimo_pagamento = '$quantia' where id_cliente = '$conta';";
$sql .= "update conta set saldo = saldo + '$quantia' where id_cliente = '$contaFav';"; 
$sql .= "update conta set ultimo_deposito = '$quantia' where id_cliente = '$contaFav';";

if(mysqli_multi_query($conexao, $sql)){
	do{

		$result = mysqli_store_result($conexao);
		mysqli_free_result($result);

	}while(mysqli_next_result($conexao));
}


$conexao->close();

if(isset($_POST['confirma'])){
	include 'encerrar.php';
}else{
	$_SESSION['sucesso_transacao'] = true;
	header('Location: transferencia.php');
}

exit;
?>