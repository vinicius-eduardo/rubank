<?php
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Cadastro - RU Bank</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/bulma.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/login.css">
</head>
<body>
	<section class="hero is-success is-fullheight">
		<div class="hero-body">
			<div class="container has-text-centered">
				<div class="column is-4 is-offset-4">
					<h3 class="title has-text-grey">Sistema de Cadastro</h3>
					<h3 class="title has-text-grey">RU Bank</h3>
					
					<?php 
						if(isset($_SESSION['status_cadastro'])):
					?>
					<div class="notification is-success">
						<p>Cadastro efetuado!</p>
						<p>Seu login é: <?php echo $_SESSION['cliente_id'];?>
						<p>E sua senha é: <?php echo $_SESSION['cliente_pass'];?></p>
						<p>Guarde essas informações!!</p>
						<p>
							Faça login informando o seu usuário e senha <a href="login.php">aqui</a>
						</p>
					</div>
					<?php
						endif;
						unset($_SESSION['status_cadastro']);
						unset($_SESSION['cliente_pass']);
					?>

					<?php 
						if(isset($_SESSION['usuario_existe'])):
					?>
					<div class="notification is-info">
						<p>
							O usuário escolhido já existe. Informe outro e tente novamente.
						</p>
					</div>
					<?php
						endif;
						unset($_SESSION['usuario_existe']);
					?>
					<div class="box">
						<form action="cadastrar.php" method="POST">

							<div class="field">
								<div class="control">
									<input name="nome" type="text" class="input is-large" placeholder="Nome Completo" autofocus>
								</div>
							</div>

							<div class="field">
								<div class="control">
									<input name="email" type="text" class="input is-large" placeholder="Email">
								</div>
							</div>

							<div class="field">
								<div class="control">
									<input name="cpf" type="text" class="input is-large" placeholder="CPF">
								</div>
							</div>

							<div class="field">
								<div class="control">
									<input name="matricula" type="text" class="input is-large" placeholder="Matrícula">
								</div>
							</div>

							<div class="field">
								<div class="control">
									<input name="senha" class="input is-large" type="password" placeholder="Senha">
								</div>
							</div>
							
							<div class="field">
								<div class="control">
									<label class="label">Categoria</label>
									<input type="radio" id="professor" name="categoria" value="professor">
									<label for="professor">Professor</label><br>
									<input type="radio" id="aluno" name="categoria" value="aluno">
									<label for="aluno">Aluno</label><br>
									<input type="radio" id="funcionario" name="categoria" value="funcionario">
									<label for="funcionario">Funcionario</label>
								</div>
							</div>

							<div class="field">
								<div class="control">
								<label class="label">Curso</label>
									<div class="select is-fullwidth">
										<select name="curso">
											<option value="">Nenhum</option>
											<option value="ciencia_computacao">Ciência da Computação</option>
											<option value="fisioterapia">Fisioterapia</option>
											<option value="medicina">Medicina</option>
											<option value="historia">História</option>
										</select>
									</div>
								</div>
							</div>

							<div class="field">
								<button type="submit" class="button is-block is-link is-large is-fullwidth">Cadastrar</button>
							</div>
							
							<div class="field">
								<a href="index.php" class="button is-fullwidth">Voltar</a>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>