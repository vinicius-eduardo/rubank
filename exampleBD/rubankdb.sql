-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 14-Abr-2020 às 07:25
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `rubankdb`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `funcao` varchar(20) NOT NULL,
  `valor` decimal(5,2) NOT NULL,
  `data_transf` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `categoria`
--

INSERT INTO `categoria` (`funcao`, `valor`, `data_transf`) VALUES
('aluno', '0.80', '2020-04-13 22:53:35'),
('funcionario', '1.00', '2020-04-13 22:53:35'),
('professor', '2.00', '2020-04-13 22:53:35');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` int(10) UNSIGNED NOT NULL,
  `nome` varchar(100) NOT NULL,
  `curso` varchar(50) NOT NULL,
  `senha` varchar(32) NOT NULL,
  `matricula` varchar(45) NOT NULL,
  `email` varchar(50) NOT NULL,
  `cpf` varchar(45) NOT NULL,
  `categoria` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nome`, `curso`, `senha`, `matricula`, `email`, `cpf`, `categoria`) VALUES
(38, 'Xenia Beatriz', 'ciencia_computacao', '202cb962ac59075b964b07152d234b70', '1234567', 'email@exemplo.com', '12345678901', 'aluno'),
(39, 'João', '', '202cb962ac59075b964b07152d234b70', '1234567', 'email@exemplo.com', '12345678901', 'funcionario'),
(40, 'Jonas', '', '202cb962ac59075b964b07152d234b70', '1234567', 'email@exemplo.com', '12345678901', 'professor');

-- --------------------------------------------------------

--
-- Estrutura da tabela `conta`
--

CREATE TABLE `conta` (
  `id_cliente` int(10) UNSIGNED NOT NULL,
  `saldo` decimal(5,2) UNSIGNED NOT NULL,
  `ultimo_pagamento` decimal(5,2) UNSIGNED NOT NULL,
  `ultimo_deposito` decimal(5,2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `conta`
--

INSERT INTO `conta` (`id_cliente`, `saldo`, `ultimo_pagamento`, `ultimo_deposito`) VALUES
(38, '15.39', '0.00', '15.39'),
(39, '50.30', '0.00', '50.30'),
(40, '12.00', '0.00', '12.00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `extrato`
--

CREATE TABLE `extrato` (
  `id_extrato` int(10) UNSIGNED NOT NULL,
  `id_cliente` int(10) UNSIGNED NOT NULL,
  `saldo` decimal(5,2) NOT NULL,
  `pagamento` decimal(5,2) NOT NULL,
  `deposito` decimal(5,2) NOT NULL,
  `data_transf` datetime NOT NULL,
  `mensagem` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `extrato`
--

INSERT INTO `extrato` (`id_extrato`, `id_cliente`, `saldo`, `pagamento`, `deposito`, `data_transf`, `mensagem`) VALUES
(357, 39, '50.30', '0.00', '50.30', '2020-04-14 02:22:57', 'Obrigado por utilizar nossos serviços!'),
(358, 40, '12.00', '0.00', '12.00', '2020-04-14 02:23:07', 'Obrigado por utilizar nossos serviços!'),
(359, 38, '15.39', '0.00', '15.39', '2020-04-14 02:23:19', 'Obrigado por utilizar nossos serviços!');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fiscal`
--

CREATE TABLE `fiscal` (
  `id_fiscal` int(10) UNSIGNED NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `senha` varchar(32) NOT NULL,
  `cpf` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `fiscal`
--

INSERT INTO `fiscal` (`id_fiscal`, `nome`, `email`, `senha`, `cpf`) VALUES
(1, 'Marcos', 'email@exemplo.com', '202cb962ac59075b964b07152d234b70', '12345678901');

-- --------------------------------------------------------

--
-- Estrutura da tabela `reembolso`
--

CREATE TABLE `reembolso` (
  `id_reembolso` int(10) UNSIGNED NOT NULL,
  `conta` int(11) NOT NULL,
  `cpf` varchar(11) NOT NULL,
  `valor` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`funcao`);

--
-- Índices para tabela `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Índices para tabela `conta`
--
ALTER TABLE `conta`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Índices para tabela `extrato`
--
ALTER TABLE `extrato`
  ADD PRIMARY KEY (`id_extrato`),
  ADD KEY `id_cliente` (`id_cliente`);

--
-- Índices para tabela `fiscal`
--
ALTER TABLE `fiscal`
  ADD PRIMARY KEY (`id_fiscal`);

--
-- Índices para tabela `reembolso`
--
ALTER TABLE `reembolso`
  ADD PRIMARY KEY (`id_reembolso`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de tabela `extrato`
--
ALTER TABLE `extrato`
  MODIFY `id_extrato` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=360;

--
-- AUTO_INCREMENT de tabela `fiscal`
--
ALTER TABLE `fiscal`
  MODIFY `id_fiscal` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `conta`
--
ALTER TABLE `conta`
  ADD CONSTRAINT `conta_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `extrato`
--
ALTER TABLE `extrato`
  ADD CONSTRAINT `extrato_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
